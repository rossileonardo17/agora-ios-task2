//
//  ElectionRow.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 14/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct ElectionRow: View {
    
    //The election that is used to populate the layout
    let election: Election
    
    var body: some View {
        
        VStack {
            
            //Row header
            HStack {
                
                Image(self.election.state)
                
                Text(self.election.name)
                    .font(.system(size: 20))
                    .fontWeight(.semibold)
                
                Spacer()
            }
            
            HStack {
                
                //Election dates
                VStack {
                    
                    //Election start date
                    HStack {
                        Image("start")
                            .resizable()
                            .frame(width: 24, height: 24)
                        Text(self.election.startDate)
                            .font(.body)
                    }
                    
                    //Election end date
                    HStack {
                        Image("finish")
                            .resizable()
                            .frame(width: 24, height: 24)
                        Text(self.election.endDate)
                            .font(.body)
                    }
                    
                }
                
                Spacer()
                
                //Election candidates
                VStack() {
                    Text(String(self.election.candidates))
                        .font(.system(size: 28))
                        .fontWeight(.bold)
                        .offset(y: 5)
                    
                    Image("candidates")
                        .offset(y: -10)
                }
                
            }
        }
        .padding(.all)
        
    }
}

struct ElectionRow_Previews: PreviewProvider {
    static var previews: some View {
        ElectionRow(election: electionsData[0])
    }
}
