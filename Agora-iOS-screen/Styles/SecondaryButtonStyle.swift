//
//  SecondaryButtonStyle.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 22/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import Foundation
import SwiftUI

struct SecondaryButtonStyle: ButtonStyle
{
    func makeBody(configuration: Self.Configuration) -> some View {
        ZStack {
            
            RoundedRectangle(cornerRadius: 10)
                .frame(height: 48)
                .foregroundColor(Color("agora-yellow"))
            
            configuration.label
                .foregroundColor(Color.white)
            
        }
    }
}
