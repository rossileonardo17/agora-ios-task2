//
//  RadioButtonStyle.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 21/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import Foundation
import SwiftUI

struct RadioButtonStyle: ButtonStyle
{
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding(.vertical, 4)
            .padding(.horizontal, 8)
            .background(Color.clear)
    }
}
