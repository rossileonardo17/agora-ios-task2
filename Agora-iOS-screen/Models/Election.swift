//
//  Election.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 13/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import Foundation


struct Election: Codable, Identifiable, Hashable {
    
    fileprivate let DATE_FORMAT = "DD MMMM yyyy"
    
    var id: Int;
    var name: String;
    var candidates: Int;
    var state: String;
    var startDate: String;
    var endDate: String;
    
}
