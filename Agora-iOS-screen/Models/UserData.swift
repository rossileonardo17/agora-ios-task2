//
//  File.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 14/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import Foundation
import SwiftUI

final class UserData: ObservableObject {
    
    @Published var elections = electionsData
    @Published var electionsBackup = electionsData
    @Published var areFiltersVisible = false
    @Published var filters: [String:String] = [:]
    
    @Published var electionsState = 0
    @Published var electionsStartDateOrder = 0
    @Published var electionsEndDateOrder = 0
}
