//
//  DataReader.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 14/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import Foundation

let electionsData: [Election] = load("elections.json")

func load<T: Decodable>(_ filename: String) -> T {
    
    let data: Data
    
    //Get a reference to the JSON file that contains the elections data
    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
    else {
        fatalError("Couldn't find \(filename) in main bundle.")
    }
    
    //Extract the data from the file
    do
    {
        data = try Data(contentsOf: file)
    }
    catch
    {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }
    
    //Parse the JSON file and return its content
    do
    {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    }
    catch
    {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
    
}
