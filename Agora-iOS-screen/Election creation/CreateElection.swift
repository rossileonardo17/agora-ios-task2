//
//  CreateElection.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 22/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct CreateElection: View {
    
    @State private var activeSection = 0;
    private var sectionIndex = 0;
    
    var body: some View {
        
        VStack(spacing: 30) {
            
            Stepper(currentSection: self.activeSection, numberOfSections: 3)
            
            if (activeSection == 0)
            {
                NewElectionMainDetails()
            }
            else if (activeSection == 1)
            {
                NewElectionCandidates()
            }
            else
            {
                NewElectionMoreOptions()
            }
            
            Spacer()
            
            HStack {
                //Back button
                if (self.activeSection > 0)
                {
                    Button(action: {
                        self.activeSection -= 1;
                    })
                    {
                        HStack {
                            Image(systemName: "chevron.left")
                            Text("Back")
                        }
                    }
                    .buttonStyle(SecondaryButtonStyle())
                }
                
                //Next button
                Button(action: {
                    self.activeSection += 1;
                })
                {
                    HStack {
                        Text("Next")
                            .fontWeight(.semibold)
                        Image(systemName: "chevron.right")
                    }
                }
                .buttonStyle(SecondaryButtonStyle())
            }
        }
        .padding()
        
    }
}

struct CreateElection_Previews: PreviewProvider {
    static var previews: some View {
        CreateElection()
    }
}
