//
//  NewElectionMainDetails.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 21/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct NewElectionMainDetails: View {
    
    @State private var electionName = "";
    @State private var electionDescription = "";
    @State private var electionStartDate = "";
    @State private var electionEndDate = "";
    
    var body: some View {
        
        VStack(spacing: 30) {

            //Election name textfield
            ZStack {
                
                RoundedRectangle(cornerRadius: 10)
                    .frame(height: 50)
                    .foregroundColor(Color("gray-backcolor"))
                
                TextField("Name", text: self.$electionName)
                    .padding([.trailing, .leading]);
            }
            
            //Election description textfield
            ZStack {
                
                RoundedRectangle(cornerRadius: 10)
                    .frame(height: 50)
                    .foregroundColor(Color("gray-backcolor"))
                
                TextField("Description", text: self.$electionDescription)
                    .padding([.trailing, .leading]);
            }
            
            //Election start date textfield
            ZStack {
                
                RoundedRectangle(cornerRadius: 10)
                    .frame(height: 50)
                    .foregroundColor(Color("gray-backcolor"))
                
                TextField("Start date", text: self.$electionStartDate)
                    .padding([.trailing, .leading]);
            }
            
            //Election end date textfield
            ZStack {
                
                RoundedRectangle(cornerRadius: 10)
                    .frame(height: 50)
                    .foregroundColor(Color("gray-backcolor"))
                
                TextField("End date", text: self.$electionEndDate)
                    .padding([.trailing, .leading]);
            }
        }
        
    }
}

struct NewElectionMainDetails_Previews: PreviewProvider {
    static var previews: some View {
        NewElectionMainDetails()
    }
}
