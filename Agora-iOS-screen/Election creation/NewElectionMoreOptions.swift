//
//  NewElectionMoreOptions.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 21/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct NewElectionMoreOptions: View {
    
    @State private var areVotersInvite = false;
    @State private var isRealTimeOne = true;
    @State private var ballotsConfIndex = 0;
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 20) {
            
            //Voting algorithm section
            VStack(alignment: .leading, spacing: 20)
            {
                Text("VOTING ALGORITHM")
                    .font(.footnote)
                    .foregroundColor(Color("agora-green"))
                
                Text("Satisfaction Approval Voting");
            }
            
            //General configuration section
            VStack(alignment: .leading, spacing: 20)
            {
                
                Text("GENERAL CONFIGURATION")
                    .font(.footnote)
                    .foregroundColor(Color("agora-green"))
                
                //Invite voters flag
                HStack
                {
                    Toggle(isOn: self.$areVotersInvite)
                    {
                        Text("Invite voters?")
                    }
                }
                
                //Real time results flag
                HStack
                {
                    Toggle(isOn: self.$isRealTimeOne)
                    {
                        Text("Get real time result?")
                    }
                }
            }
            
            //Ballots configuration section
            VStack(alignment: .leading, spacing: 20)
            {
                Text("BALLOTS CONFIGURATION")
                    .font(.footnote)
                    .foregroundColor(Color("agora-green"))
            
                RadioGroup(children: Constants.BALLOTS_MORE_OPTIONS)
            }
            
            //List of voters configuration section
            VStack(alignment: .leading, spacing: 20)
            {
                Text("LIST OF BALLOTS CONFIGURATION")
                    .font(.footnote)
                .foregroundColor(Color("agora-green"))
                
                RadioGroup(children: Constants.LIST_VOTERS_MORE_OPTIONS)
            }
            
        }
        
    }
}

struct NewElectionMoreOptions_Previews: PreviewProvider {
    static var previews: some View {
        NewElectionMoreOptions()
    }
}
