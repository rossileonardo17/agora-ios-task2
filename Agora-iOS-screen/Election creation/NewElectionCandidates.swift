//
//  NewElectionCandidates.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 21/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct NewElectionCandidates: View {
    
    @State private var candidateName = "";
    
    var body: some View {
        
        VStack {
            
            //Candidates name
            HStack {
                
                ZStack {
                    
                    RoundedRectangle(cornerRadius: 10)
                        .frame(height: 50)
                        .foregroundColor(Color("gray-backcolor"))
                    
                    TextField("Name", text: self.$candidateName)
                        .padding([.trailing, .leading]);
                }
                
                Button(action: {}) {
                    Image(systemName: "plus")
                    .font(.system(size: 30))
                }
                
            }
            
            List {
                Text("Candidate 1")
                Text("Candidate 2")
                Text("Candidate 3")
            }
            
        }
        
    }
}

struct NewElectionCandidates_Previews: PreviewProvider {
    static var previews: some View {
        NewElectionCandidates()
    }
}
