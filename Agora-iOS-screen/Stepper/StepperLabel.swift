//
//  StepperLabel.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 22/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct StepperLabel: View {
    
    let index: Int;
    let text: String;
    
    var body: some View {
        
        HStack {
            ZStack {
                Circle()
                    .fill(Color("agora-yellow").opacity(0.3))
                    .frame(width: 25, height: 25)
                
                Text(String(self.index + 1))
                    .font(.system(size: 20))
                    .fontWeight(.semibold)
                    .foregroundColor(Color("agora-yellow"))
            }
            
            Text(self.text)
                .font(.system(size: 20))
                .foregroundColor(Color("agora-yellow"))
            
        }

        
    }
}

struct StepperLabel_Previews: PreviewProvider {
    static var previews: some View {
        StepperLabel(index: 0, text: "Test")
    }
}
