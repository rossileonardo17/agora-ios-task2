//
//  Stepper.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 22/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct Stepper: View {
    
    let currentSection: Int;
    let numberOfSections: Int;
    
    var body: some View {
        
        Group {
            
            GeometryReader { hStackReader in
                
                if (self.currentSection == 0)
                {
                    //Here two parts have to be created: the first one with the name of the section
                    //and the second one that contains a line
                    HStack{
                        
                        StepperLabel(index: self.currentSection, text: Constants.NEW_ELECTION_SECTIONS[self.currentSection])
                            
                        Path { path in
                            path.move(to: CGPoint(x: 0, y: hStackReader.size.height / 2))
                            path.addLine(to: CGPoint(x: hStackReader.size.width, y: hStackReader.size.height / 2))
                        }
                        .stroke(Color("agora-yellow").opacity(0.3), lineWidth: 2)
                    }
                    
                }
                else if (self.currentSection == self.numberOfSections - 1)
                {
                    //Here two parts have to be designed: the first one a line and the second one
                    //with the name of the section
                    HStack{
                        Group {
                            GeometryReader { lineReader in
                                Path { path in
                                    path.move(to: CGPoint(x: 0, y: hStackReader.size.height / 2))
                                    path.addLine(to: CGPoint(x: lineReader.size.width, y: hStackReader.size.height / 2))
                                }
                                .stroke(Color("agora-yellow").opacity(0.3), lineWidth: 2)
                            }
                        }
                        
                        StepperLabel(index: self.currentSection, text: Constants.NEW_ELECTION_SECTIONS[self.currentSection])
                    }
                }
                else
                {
                    //Here three parts have to be created: two lines and in the middle the one
                    //with the name of the section
                    HStack {
                        Group {
                             GeometryReader { lineReader in
                                 Path { path in
                                     path.move(to: CGPoint(x: 0, y: hStackReader.size.height / 2))
                                     path.addLine(to: CGPoint(x: lineReader.size.width, y: hStackReader.size.height / 2))
                                 }
                                 .stroke(Color("agora-yellow").opacity(0.3), lineWidth: 2)
                             }
                         }
                        
                        StepperLabel(index: self.currentSection, text: Constants.NEW_ELECTION_SECTIONS[self.currentSection])
                        
                        Group {
                             GeometryReader { lineReader in
                                 Path { path in
                                     path.move(to: CGPoint(x: 0, y: hStackReader.size.height / 2))
                                     path.addLine(to: CGPoint(x: lineReader.size.width, y: hStackReader.size.height / 2))
                                 }
                                 .stroke(Color("agora-yellow").opacity(0.3), lineWidth: 2)
                             }
                         }
                    }
                }
                
            }
            
        }
        .frame(height: 40)
        
    }
}

struct Stepper_Previews: PreviewProvider {
    static var previews: some View {
        Stepper(currentSection: 1, numberOfSections: 3)
    }
}

