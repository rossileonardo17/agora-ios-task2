//
//  Constants.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 22/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import Foundation

struct Constants {
    
    static let BALLOTS_MORE_OPTIONS = [
        "Ballots are completely secret and never shown to everyone",
        "Ballots are visible only to me",
        "Ballots are visible to everyone with access to the election"
    ];
    
    static let LIST_VOTERS_MORE_OPTIONS = [
        "Only to me",
        "Everyone with access to election"
    ];
    
    static let NEW_ELECTION_SECTIONS = [
        "Main details",
        "Candidates",
        "More options"
    ];
}
