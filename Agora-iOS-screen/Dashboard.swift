//
//  ContentView.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 13/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct Dashboard: View {

    @EnvironmentObject private var userData: UserData;
    
    var body: some View {
        
        ZStack {
            
            //Dashboard content
            VStack {
                
                //Page header
                HStack {
                    
                    Text("Upcoming election")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                    
                    Spacer()
                    
                    Button(action: {})
                    {
                        Image(systemName: "plus")
                            .font(.system(size: 30))
                    }
                }
                .padding([.leading, .trailing])
                
                //Upcoming election
                UpcomingElectionView()
                    .padding([.leading, .bottom, .trailing])
                
                //Filter section
                HStack {
                    
                    ForEach(self.userData.filters.keys.sorted(), id: \.self) { key in
                        ChipFilter(filterDescr: self.userData.filters[key]!)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.userData.areFiltersVisible.toggle()
                    })
                    {
                        ZStack {
                            
                            RoundedRectangle(cornerRadius: 30)
                                .frame(width: 110, height: 28)
                                .foregroundColor(Color("gray-backcolor"))
                            
                            HStack {
            
                                Image(systemName: "list.bullet")
                                Text("FILTERS")
                                    .font(.body)
            
                            }
                        }
                    }
                    
                }
                .padding([.trailing, .leading, .bottom])
                
                //Election list
                List {
                    ForEach(self.userData.elections) { election in
                        
                        if self.getStateByIndex(index: self.userData.electionsState) != "none"
                        {
                            if election.state == self.getStateByIndex(index: self.userData.electionsState)
                            {
                                ElectionRow(election: election)
                            }
                        }
                        else
                        {
                            ElectionRow(election: election)
                        }
                        
                    }
                }
        
            }
            
            //Filter action sheet
            VStack {
                Spacer()
                DashboardFilters()
                    .environmentObject(self.userData)
                    .offset(y: self.userData.areFiltersVisible ? 0 : UIScreen.main.bounds.height)
            }
            .background(self.userData.areFiltersVisible ? Color.black.opacity(0.3) : Color.clear)
            .edgesIgnoringSafeArea(.all)
        }
        .animation(.default)
        
    }
}

extension Dashboard {
    
    func getStateByIndex(index: Int) -> String {

        switch index {
        case 0:
            return "none"
        case 1:
            return "pending"
        case 2:
            return "on_going"
        case 3:
            return "finished"
        default:
            return "none"
        }
        
    }
    
}


struct Dashboard_Previews: PreviewProvider {
    static var previews: some View {
        Dashboard()
            .environmentObject(UserData())
    }
}
