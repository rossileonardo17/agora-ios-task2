//
//  UpcomingElectionView.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 13/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct UpcomingElectionView: View {
    
    
    var body: some View {
        
        ZStack {
            
            //Card frame
            RoundedRectangle(cornerRadius: 20)
            .frame(height: 170)
            .shadow(color: Color("upcoming-election-shadow"), radius: 20, x: 0, y: 4)
            .foregroundColor(Color.white)
            
            //Card content that has: the header and the details of the upcoming election
            VStack {
                
                //Card header
                HStack {
                    
                    Image("pin-up-el")
                    
                    Text("Students representative election")
                        .font(.system(size: 20))
                        .fontWeight(.bold)
                        .foregroundColor(Color("upcoming-election-text"))
                    
                    Spacer()
                }
                
                //Card body
                HStack() {
                    
                    //Election start date
                    VStack(alignment: .center) {
                        Image("start")
                            .resizable()
                            .frame(width: 30, height: 30)
                            
                        Text("03:30 PM")
                            .font(.body)
                            .fontWeight(.bold)
                            .foregroundColor(Color("upcoming-election-text"))
                        
                        Text("14 Mar 2020")
                            .font(.body)
                            .fontWeight(.bold)
                            .foregroundColor(Color("upcoming-election-text"))
                    }
                    
                    Spacer()
                    
                    //Election end date
                    VStack(alignment: .center) {
                        Image("finish")
                            .resizable()
                            .frame(width: 30, height: 30)
                        
                        Text("03:30 PM")
                            .fontWeight(.bold)
                            .foregroundColor(Color("upcoming-election-text"))
                        
                        Text("14 Mar 2020")
                            .fontWeight(.bold)
                            .foregroundColor(Color("upcoming-election-text"))
                    }
                    
                    Spacer()
                    
                    //Election candidates
                    VStack(alignment: .center) {
                        Text("3")
                            .font(.largeTitle)
                            .fontWeight(.bold)
                            .foregroundColor(Color("upcoming-election-text"))
                        
                        Image("candidates")
                            .resizable()
                            .frame(width: 40, height: 20)
                            .offset(y: -15)
                        
                    }
                    
                }
                .padding(.horizontal, 10.0)
            }
            .padding()
        }
    }
}

struct UpcomingElectionView_Previews: PreviewProvider {
    static var previews: some View {
        UpcomingElectionView()
    }
}
