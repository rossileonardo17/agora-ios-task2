//
//  RadioButton.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 21/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct RadioButton: View {
    
    let id: Int;
    let text: String;
    @Binding var parentStates: [Bool];
    
    var body: some View {
        
        Button(action: {
            let activeState = self.parentStates.firstIndex(of: true)!;
            self.parentStates[activeState].toggle()
            self.parentStates[self.id].toggle()
        })
        {
            HStack(alignment: .top) {
                
                ZStack {
                    
                    Circle()
                        .frame(width: 20, height: 20)
                        .foregroundColor(Color.clear)
                        .overlay(
                            RoundedRectangle(cornerRadius: 16)
                                .stroke(Color("agora-green"), lineWidth: 3)
                        )
                    
                    Circle()
                        .fill(self.parentStates[self.id] ? Color("agora-green") : Color.clear)
                        .frame(width: 12, height: 12)
                }
                
                Text(self.text)
                    .foregroundColor(Color.black)
                    .font(.system(size: 20))
            }
        }
        .buttonStyle(RadioButtonStyle())
        
    }
}

struct RadioButton_Previews: PreviewProvider {
    static var previews: some View {
        RadioButton(id: 0, text: "Test", parentStates: .constant([true, false, false]))
    }
}
