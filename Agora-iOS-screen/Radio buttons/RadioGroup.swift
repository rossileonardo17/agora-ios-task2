//
//  RadioGroup.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 22/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct RadioGroup: View {
    
    let childrenLabels: [String];
    @State var childrenStates: [Bool] = [];
    
    init(children: [String]) {
        
        //First of all the children labels are stored
        self.childrenLabels = children;
        
        //Define the initial states of the group: the first element is active
        _childrenStates = State(initialValue: self.computeGroupInitialStates())
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            ForEach(0 ..< self.childrenLabels.count, id: \.self){ index in
                RadioButton(id: index, text: self.childrenLabels[index], parentStates: self.$childrenStates)
            }
        }
    }
}

extension RadioGroup {
    
    /**
     Compute the group initial states such that the first element is the one that is selected
     
     - returns:
     An array of bool that represents the group states. An element is true if selected, false otherwise
     */
    private func computeGroupInitialStates() -> [Bool]
    {
        var initialStates: [Bool] = []
        for index in 0 ..< self.childrenLabels.count
        {
            let state = index == 0 ? true : false;
            initialStates.append(state);
        }
        
        return initialStates;
    }
    
}

struct RadioGroup_Previews: PreviewProvider {
    static var previews: some View {
        RadioGroup(children: ["Option 1", "Option 2", "Option 3"])
    }
}
