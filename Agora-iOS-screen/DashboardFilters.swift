//
//  DashboardFilters.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 14/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct DashboardFilters: View {
    
    @EnvironmentObject var userData: UserData;
    
    @State private var selectedStatus = 0
    @State private var selectedStart = 0
    @State private var selectedEnd = 0
    
    private let statuses = ["All", "Pending", "On going", "Finished"];
    private let orders = ["None", "Asc", "Desc"]
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 20) {
        
            //Filters view title
            Text("Filter your elections")
                .font(.title)
                .fontWeight(.bold)
            
            //Status filtering
            VStack(alignment: .leading, spacing: 15) {
                
                Text("FILTER BY")
                    .foregroundColor(Color("agora-green"))
                
                //Status picker row
                HStack {
                    
                    Text("Status")
                    
                    Picker("Test", selection: self.$selectedStatus) {
                        
                        ForEach(0 ..< self.statuses.count) { index in
                            Text(self.statuses[index]).tag(index)
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
            }
            
            
            //Date filtering
            VStack(alignment: .leading, spacing: 15) {
                
                Text("SORT BY")
                    .foregroundColor(Color("agora-green"))
                
                //Start date picker row
                HStack {
                    
                    Text("Start date")
                    
                    Picker("Start date", selection: self.$selectedStart) {
                        ForEach(0 ..< self.orders.count) { index in
                            Text(self.orders[index]).tag(index)
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .disabled(self.selectedEnd > 0)

                }
                
                //End date picker row
                HStack {
                    
                    Text("Start date")
                    
                    Picker("Start date", selection: self.$selectedEnd) {
                        ForEach(0 ..< self.orders.count) { index in
                            Text(self.orders[index]).tag(index)
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .disabled(self.selectedStart > 0)

                }
            }
            
            
            //Buttons section
            VStack(spacing: 15) {
                
                //Apply filters button
                Button(action: {
                    
                    //Update the user data so that the view can be correctly updated
                    self.userData.electionsState = self.selectedStatus
                    self.userData.electionsStartDateOrder = self.selectedStart
                    self.userData.electionsEndDateOrder = self.selectedEnd
                    
                    
                    //Toggle the filters flag so that the view can be closed
                    self.userData.areFiltersVisible.toggle()
                    
                    if self.selectedEnd == 0
                    {
                        self.handleStartDateOrdering()
                    }
                    else
                    {
                        self.handleEndDateOrdering()
                    }
                    
                    
                    self.handleStatusFilter()
                    
                })
                {
                    ZStack {
                        
                        RoundedRectangle(cornerRadius: 10)
                        .frame(height: 48)
                        .foregroundColor(Color("agora-yellow"))
                        
                        Text("Apply filters")
                            .foregroundColor(.white)
                            .fontWeight(.semibold)
                    }
                }
                
                //Cancel button
                 Button(action: {
                    self.userData.areFiltersVisible.toggle()
                 })
                 {
                     ZStack {
                         
                         RoundedRectangle(cornerRadius: 10)
                             .frame(height: 48)
                             .foregroundColor(.red)
                         
                         Text("Cancel")
                             .foregroundColor(.white)
                             .fontWeight(.semibold)
                     }
                 }
            }
            .offset(y: 20)
            
        }
        .padding(.bottom, (UIApplication.shared.windows.last?.safeAreaInsets.bottom)! + 10)
        .padding(.horizontal, 30)
        .padding(.top, 30)
        .background(Color.white)
        .cornerRadius(30)
        .edgesIgnoringSafeArea(.bottom)
    }
}

extension DashboardFilters {
    
    private func handleStartDateOrdering()
    {
        self.userData.filters["endDate"] = nil
        
        if self.selectedStart == 1
        {
            self.userData.filters["startDate"] = "START: ASC"
            self.userData.elections = self.userData.elections.sorted(by: { $0.startDate < $1.startDate })
        }
        else if self.selectedStart == 2
        {
            self.userData.filters["startDate"] = "START: DESC"
            self.userData.elections = self.userData.elections.sorted(by: { $0.startDate > $1.startDate })
        }
        else
        {
            self.userData.filters["startDate"] = nil
            self.userData.elections = self.userData.electionsBackup
        }
    }
    
    private func handleEndDateOrdering()
    {
        self.userData.filters["startDate"] = nil
        
        if self.selectedEnd == 1
        {
            self.userData.filters["endDate"] = "END: ASC"
            self.userData.elections = self.userData.elections.sorted(by: { $0.endDate < $1.endDate })
        }
        else if self.selectedStart == 2
        {
            self.userData.filters["endDate"] = "END: ASC"
            self.userData.elections = self.userData.elections.sorted(by: { $0.endDate > $1.endDate })
        }
        else
        {
            self.userData.filters["endDate"] = nil
            self.userData.elections = self.userData.electionsBackup
        }
    }
    
    private func handleStatusFilter()
    {
        switch self.selectedStatus {
        case 0:
            self.userData.filters["status"] = nil
        case 1:
            self.userData.filters["status"] = "PENDING"
        case 2:
            self.userData.filters["status"] = "ON GOING"
        case 3:
            self.userData.filters["status"] = "FINISHED"
        default:
            self.userData.filters["status"] = nil
        }
    }
    
}

struct DashboardFilters_Previews: PreviewProvider {
    static var previews: some View {
        DashboardFilters()
            .environmentObject(UserData())
    }
}
