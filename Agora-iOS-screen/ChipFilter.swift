//
//  ChipFilter.swift
//  Agora-iOS-screen
//
//  Created by Leonardo Rossi on 14/03/2020.
//  Copyright © 2020 Leonardo Rossi. All rights reserved.
//

import SwiftUI

struct ChipFilter: View {
    
    let filterDescr: String;
    
    var body: some View {
        
        ZStack {
            
            RoundedRectangle(cornerRadius: 30)
                .frame(width: 110, height: 28)
                .foregroundColor(Color("agora-green").opacity(0.3))
            
            Text(self.filterDescr)
                .foregroundColor(Color("agora-green"))
                .fontWeight(.semibold)
                .font(.caption)
            
        }
        
    }
}

struct ChipFilter_Previews: PreviewProvider {
    static var previews: some View {
        ChipFilter(filterDescr: "FILTER")
    }
}
